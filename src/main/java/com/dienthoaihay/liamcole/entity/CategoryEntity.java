package com.dienthoaihay.liamcole.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="categories")
public class CategoryEntity extends BaseEntity{
    @Column
    private String name;

    @Column
    private String code;

    public CategoryEntity(){
        super();
    }
    public CategoryEntity(Long id,String name, String code) {
        super();
        this.name = name;
        this.code = code;
    }

    @OneToMany(mappedBy = "categories")


    private List<ProductEntity> products = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<ProductEntity> getProducts() {
        return products;
    }

    public void setProducts(List<ProductEntity> products) {
        this.products = products;
    }
}
