package com.dienthoaihay.liamcole.entity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name="products")
public class ProductEntity extends BaseEntity {
    @Column

    private String name;

    @Column

    private String code;

    @Column
    private String thumbnail;

    @Column
    private String price;

    @ManyToOne
    private CategoryEntity categories;

    public ProductEntity(){
        super();
    }
    public ProductEntity(Long id, String name, String code, String thumbnail, String price, CategoryEntity categories) {
        super();
        this.setId(id);
        this.name = name;
        this.code = code;
        this.thumbnail = thumbnail;
        this.price = price;
        this.categories = categories;
//        this.tags = tags;
    }
    public ProductEntity( String name, String code, String thumbnail, String price) {
        super();
        this.name = name;
        this.code = code;
        this.thumbnail = thumbnail;
        this.price = price;
//        this.tags = tags;
    }
    @ManyToMany(cascade = {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
    @JoinTable(name = "product_tags",
            joinColumns = @JoinColumn(name = "product_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )

    private List<TagEntity> tags = new ArrayList<>();

    public List<TagEntity> getTags() {
        return tags;
    }

    public void setTags(List<TagEntity> tags) {
        this.tags = tags;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }


    public CategoryEntity getCategories() {
        return categories;
    }

    public void setCategories(CategoryEntity categories) {
        this.categories = categories;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}