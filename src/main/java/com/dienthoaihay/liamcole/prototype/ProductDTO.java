package com.dienthoaihay.liamcole.prototype;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;


public class ProductDTO extends BaseDTO<ProductDTO>{
    @NotBlank(message = "name không được để trống")
//    @Email(message = "Name phải là Email ?")
    private String name;
//    @NotBlank(message = "code không được để trống")
    private String code;
    @NotBlank(message = "thumbnail không được để trống")
    private String thumbnail;
    @NotBlank(message = "price không được để trống")
    private String price;
    @NotBlank(message = "categoryCode không được để trống")
    private String categoryCode;
//    @NotBlank(message = "idTags không được để trống")
    private Long[] idTags;

    public Long[] getIdTags() {
        return idTags;
    }

    public ProductDTO(){
        super();
    }
    public ProductDTO( Long id,String name, String code, String thumbnail, String price, String categoryCode, Long[] idTags) {
        super();
        this.setId(id);
        this.name = name;
        this.code = code;
        this.thumbnail = thumbnail;
        this.price = price;
        this.categoryCode = categoryCode;
        this.idTags = idTags;
    } public ProductDTO( String name, String code, String thumbnail, String price, String categoryCode, Long[] idTags) {
        super();
        this.name = name;
        this.code = code;
        this.thumbnail = thumbnail;
        this.price = price;
        this.categoryCode = categoryCode;
        this.idTags = idTags;
    }

    public void setIdTags(Long[] idTags) {
        this.idTags = idTags;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }


}
