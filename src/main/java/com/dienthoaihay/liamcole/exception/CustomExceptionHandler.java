package com.dienthoaihay.liamcole.exception;

import org.hibernate.TypeMismatchException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.server.ResponseStatusException;

@RestControllerAdvice //  cho phép có thể trả về view thay vì json
public class CustomExceptionHandler {
    // error không tồn tại
    @ExceptionHandler(NotFoundException.class) // chỉ rõ method này sử lý exception nào
    @ResponseStatus(HttpStatus.NOT_FOUND) // định nghĩa Http method return client
    public ErrorResponse handlerNotFoundException(NotFoundException ex, WebRequest req){
        return new ErrorResponse(HttpStatus.NOT_FOUND,ex.getMessage());
    }
    // lỗi trùng lặp dữ liệu
    @ExceptionHandler(DuplicateRecordException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handlerDuplicateRecordException(DuplicateRecordException ex, WebRequest req) {
        // Log err

        return new ErrorResponse(HttpStatus.BAD_REQUEST, ex.getMessage());
    }
//
    // Xử lý tất cả các exception chưa được khai báo
//    @ExceptionHandler(Exception.class)
//    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
//    public ErrorResponse handlerException(Exception ex, WebRequest req) {
//        // Log err
//
//        return new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
//    }

    // error dữ liệu NULL
    @ExceptionHandler(NullPointerException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse handlerNullPointerException(NullPointerException ex, WebRequest req){
        return new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR,ex.getMessage());
    }


//    VALDIDATE
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<?> customValidationErrorHanding(MethodArgumentNotValidException exception){
        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.BAD_REQUEST,exception.getBindingResult().getFieldError().getDefaultMessage());
        return new ResponseEntity<>(errorResponse,HttpStatus.BAD_REQUEST);
    }


}
