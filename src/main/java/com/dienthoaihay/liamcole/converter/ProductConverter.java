package com.dienthoaihay.liamcole.converter;

import com.dienthoaihay.liamcole.entity.ProductEntity;
import com.dienthoaihay.liamcole.entity.TagEntity;
import com.dienthoaihay.liamcole.exception.NotFoundException;
import com.dienthoaihay.liamcole.helper.Helper;
import com.dienthoaihay.liamcole.prototype.ProductDTO;
import com.dienthoaihay.liamcole.repository.CategoryRepository;
import com.dienthoaihay.liamcole.repository.ProductRepository;
import com.dienthoaihay.liamcole.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.text.html.parser.Entity;
import java.util.List;
import java.util.stream.Collectors;


@Component
public class ProductConverter {
    @Autowired
    private Helper helper;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private TagRepository tagRepository;
    public ProductEntity toEntity(ProductDTO dto){
        ProductEntity productEntity = new ProductEntity();
        productEntity.setName(dto.getName());
        productEntity.setCode(helper.toSlug(dto.getName()));
        productEntity.setThumbnail(dto.getThumbnail());
        productEntity.setPrice(dto.getPrice());
        productEntity.setCategories(categoryRepository.findOneByCode(dto.getCategoryCode()));
        for(int i =0; i<dto.getIdTags().length;i++){
            Long k = dto.getIdTags()[i];
            TagEntity tag = tagRepository.findOneById(k);
            productEntity.getTags().add(tag);
        }
        // set table trung gian
        return productEntity;
    }
    public ProductDTO toDTO(ProductEntity entity){
        ProductDTO productDTO = new ProductDTO();
        if(entity.getId() != null) {
            productDTO.setId(entity.getId());
        }
        productDTO.setName(entity.getName());
        productDTO.setCode(entity.getCode());
        productDTO.setThumbnail(entity.getThumbnail());
        productDTO.setPrice(entity.getPrice());
        if(entity.getCategories() != null) {
            productDTO.setCategoryCode(entity.getCategories().getCode());
        }
        else
        {
            throw new NotFoundException("Category không được để trống !");
        }
//        List<TagEntity> ls = entity.getTags().stream().limit(entity.getTags().size()).collect(Collectors.toList());
//        Long[] arrIdTags = new Long[100];
//        if(entity.getTags().size() >0){
//            for(int i = 0; i<entity.getTags().size();i++){
//                arrIdTags[i] = entity.getTags().get(i).getId();
//            }
//            productDTO.setIdTags(arrIdTags);
//        }

        productDTO.setCreatedDate(entity.getCreatedDate());
        productDTO.setCreatedBy(entity.getCreatedBy());
        productDTO.setModifiedBy(entity.getModifiedBy());
        productDTO.setModifiedDate(entity.getModifiedDate());
        return productDTO;
    }
    public ProductEntity toEntity(ProductDTO dto,ProductEntity oldEntity){
        oldEntity.setName(dto.getName());
        oldEntity.setCode(helper.toSlug(dto.getName()));
        oldEntity.setThumbnail(dto.getThumbnail());
        oldEntity.setPrice(dto.getPrice());
        return oldEntity;
    }
}
