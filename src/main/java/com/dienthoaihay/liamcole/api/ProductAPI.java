package com.dienthoaihay.liamcole.api;

import com.dienthoaihay.liamcole.api.output.ProductOutput;
import com.dienthoaihay.liamcole.exception.NotFoundException;
import com.dienthoaihay.liamcole.prototype.ProductDTO;
import com.dienthoaihay.liamcole.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin // là một tiêu chuẩn bảo mật cho mobile
@RestController
public class ProductAPI {

    @Autowired
    private IProductService productService;

    @PostMapping(value = "/api/product")
    public ResponseEntity<ProductDTO> createProduct(@Valid @RequestBody ProductDTO model) {

        return ResponseEntity.status(HttpStatus.CREATED).body(productService.save(model));
    }

    @PutMapping(value = "/api/product/{id}")
    public ResponseEntity<ProductDTO> updateProduct(@RequestBody ProductDTO model, @PathVariable("id") long id) {
        model.setId(id);
        return ResponseEntity.status(HttpStatus.OK).body(productService.save(model));
    }

    @GetMapping(value = "/api/product")
    public ResponseEntity<ProductOutput> showNew(@RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "limit", required = false) Integer limit) {
        ProductOutput result = new ProductOutput();
        // nếu sử dụng pagination
        if (page != null && limit != null) {
            result.setPage(page);
            Pageable pageable = PageRequest.of(page - 1, limit, Sort.by("id"));
            result.setListResult(productService.findAll(pageable));
            result.setTotalPage((int) Math.ceil((double) (productService.totalItem()) / limit));
        } else {
            productService.findAll();
            result.setListResult(productService.findAll());
        }
        return ResponseEntity.status(HttpStatus.OK).body(result);

    }
}
