package com.dienthoaihay.liamcole.api.output;

import com.dienthoaihay.liamcole.prototype.ProductDTO;

import java.util.ArrayList;
import java.util.List;

public class ProductOutput {
    private int page; // current page - client

    private int totalPage; // total page - client

    private List<ProductDTO> listResult = new ArrayList<>();

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public List<ProductDTO> getListResult() {
        return listResult;
    }

    public void setListResult(List<ProductDTO> listResult) {
        this.listResult = listResult;
    }
}
