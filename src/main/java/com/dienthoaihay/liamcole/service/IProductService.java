package com.dienthoaihay.liamcole.service;

import com.dienthoaihay.liamcole.prototype.ProductDTO;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IProductService {
    ProductDTO save(ProductDTO productDTO);
    List<ProductDTO> findAll(Pageable pageable);
    List<ProductDTO> findAll();
    int totalItem();
}
