package com.dienthoaihay.liamcole.service.impl;

import com.dienthoaihay.liamcole.converter.ProductConverter;
import com.dienthoaihay.liamcole.entity.ProductEntity;
import com.dienthoaihay.liamcole.entity.TagEntity;
import com.dienthoaihay.liamcole.exception.NotFoundException;
import com.dienthoaihay.liamcole.helper.Helper;
import com.dienthoaihay.liamcole.prototype.ProductDTO;
import com.dienthoaihay.liamcole.repository.CategoryRepository;
import com.dienthoaihay.liamcole.repository.ProductRepository;
import com.dienthoaihay.liamcole.repository.TagRepository;
import com.dienthoaihay.liamcole.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService implements IProductService {

    @Autowired
    private ProductConverter productConverter;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private TagRepository tagRepository;
    @Override
    public ProductDTO save(ProductDTO productDTO) {
        ProductEntity productEntity = new ProductEntity();
        if(productDTO.getId() != null){
            ProductEntity oldProductEntity = productRepository.findOneById(productDTO.getId());
            productEntity = productConverter.toEntity(productDTO,oldProductEntity);
        }
        else{
            productEntity = productConverter.toEntity(productDTO);
        }
        productEntity = productRepository.save(productEntity);
        return productConverter.toDTO(productEntity);
    }

    @Override
    public List<ProductDTO> findAll(Pageable pageable) {
        List<ProductDTO> results = new ArrayList<>();
        List<ProductEntity> entities = productRepository.findAll(pageable).getContent();
        for(ProductEntity item: entities){
            ProductDTO productDTO = productConverter.toDTO(item);
            results.add(productDTO);
        }
        return results;
    }

    @Override
    public List<ProductDTO> findAll() {
        List<ProductDTO> results = new ArrayList<>();
        List<ProductEntity> entities = productRepository.findAll();
        for(ProductEntity item: entities){
            ProductDTO productDTO = productConverter.toDTO(item);
            results.add(productDTO);
        }
        return results;
    }

    @Override
    public int totalItem() {
        return (int) productRepository.count();
    }
}
