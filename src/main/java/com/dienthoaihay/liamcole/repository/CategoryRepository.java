package com.dienthoaihay.liamcole.repository;

import com.dienthoaihay.liamcole.entity.CategoryEntity;
import com.dienthoaihay.liamcole.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<CategoryEntity,Long> {
    CategoryEntity findOneByCode(String code);
}
