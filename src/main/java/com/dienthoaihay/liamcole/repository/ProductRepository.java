package com.dienthoaihay.liamcole.repository;

import com.dienthoaihay.liamcole.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ProductRepository extends JpaRepository<ProductEntity,Long> {
    ProductEntity findOneById(Long id);
//    @Query("select id from tags")
}
