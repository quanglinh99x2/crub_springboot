package com.dienthoaihay.liamcole.repository;

import com.dienthoaihay.liamcole.entity.TagEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TagRepository extends JpaRepository<TagEntity,Long> {
    TagEntity findOneById(Long i);
}
