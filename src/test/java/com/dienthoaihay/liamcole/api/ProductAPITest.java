package com.dienthoaihay.liamcole.api;
import com.dienthoaihay.liamcole.api.output.ProductOutput;
import com.dienthoaihay.liamcole.converter.BaseConvert;
import com.dienthoaihay.liamcole.converter.ProductConverter;
import com.dienthoaihay.liamcole.entity.CategoryEntity;
import com.dienthoaihay.liamcole.entity.ProductEntity;
import com.dienthoaihay.liamcole.prototype.ProductDTO;
import com.dienthoaihay.liamcole.repository.CategoryRepository;
import com.dienthoaihay.liamcole.service.impl.ProductService;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class) // ket hop spring va junit tạo ApplicationContext và các bean
@SpringBootTest  // dua bean vao trong context.sẽ đi tìm kiếm class có gắn @SpringBootApplication và từ đó đi tìm toàn bộ Bean và nạp vào Context
@AutoConfigureMockMvc
//@TestConfiguration // tiet kiem tai nguyen hon vi chi tao ra bean trong config
public class ProductAPITest {

    @Autowired
    private MockMvc mvc;
    @Autowired
    private ProductConverter productConverter;
    @Autowired
    private BaseConvert baseConvert;
    @MockBean
    private ProductService productService;
    @MockBean
    private CategoryRepository categoryRepository;

    private List<ProductDTO> listProduct = new ArrayList<>();
    @BeforeEach
    public void init(){
    }
    // TEST CREATE ALL CREATE
    @Test
    public void createProdict() throws Exception{
        CategoryEntity categoryEntity = new CategoryEntity(1L,"Điện thoại","dien-thoai");
        Long[] idTags = {1L,2L};

        ProductDTO productDTO = new ProductDTO("san pham 1","san-pahm-1","images/hjshdjfh23.jpg","213213","laptop",idTags);

        given(productService.save(any())).willReturn(productDTO);

        String inputJson = baseConvert.mapToJson(productDTO);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/api/product")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        int status = mvcResult.getResponse().getStatus(); // need status :200
        Assert.assertEquals(HttpStatus.CREATED.value(), status); // kiểm tra trạng thái

        String content = mvcResult.getResponse().getContentAsString();
        ProductDTO pro = baseConvert.mapFromJson(content,ProductDTO.class);
        Assert.assertEquals("san pham 1",pro.getName());
    }

    // TEST FETCH ALL PRODUCT
    @Test
    public void getAllProudct() throws Exception{
        CategoryEntity categoryEntity = new CategoryEntity(1L,"Điện thoại","dien-thoai");
        List<ProductEntity> allProducts = IntStream.range(0, 10).mapToObj(i -> new ProductEntity(2L,"iphone 12","iphone-12","hello","2321321",categoryEntity))
                .collect(Collectors.toList());
        for (ProductEntity item : allProducts) {
            ProductDTO dto = productConverter.toDTO(item);
            listProduct.add(dto);
        }
        given(productService.findAll()).willReturn(listProduct);
        mvc.perform(MockMvcRequestBuilders.get("/api/product"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.listResult[0].name",is("iphone 12")));

    }
    //TEST GET ALL WITH PAGINATION
    @Test
    public void getAllProductWithPaginate() throws Exception{

        CategoryEntity categoryEntity = new CategoryEntity(1L,"Điện thoại","dien-thoai");
        List<ProductEntity> allProducts = IntStream.range(0, 10).mapToObj(i -> new ProductEntity((long)i,"iphone 12-"+i,"iphone-12","hello","2321321",categoryEntity))
                .collect(Collectors.toList());

        for (ProductEntity item : allProducts) {
            ProductDTO dto = productConverter.toDTO(item);
            listProduct.add(dto);
        }
        List<ProductDTO> get3 = listProduct.stream().limit(3).collect(Collectors.toList());

        given(productService.findAll(any())).willReturn(get3);

        mvc.perform(MockMvcRequestBuilders.get("/api/product?page=1&limit=3"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.totalPage").isNumber())
//                .andExpect("$.listResult[0])
                .andExpect(jsonPath("$.listResult[0].name",is("iphone 12-0")));
    }
    // TEST UPDATE
    @Test
    public void updateProduct() throws Exception{
//        CategoryEntity categoryEntity = new CategoryEntity(1L,"Điện thoại","dien-thoai");
        Long[] idTags = {1L,2L};

        ProductDTO productDTO = new ProductDTO("san pham 1","san-pahm-1","images/hjshdjfh23.jpg","213213","laptop",idTags);
        productDTO.setId(1L);
//        Mockito.when(productService.save(productDTO)).thenReturn(productDTO);
        given(productService.save(any())).willReturn(productDTO);
        String jsonInput = baseConvert.mapToJson(productDTO);
        MvcResult result = mvc.perform(MockMvcRequestBuilders.put("/api/product/1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(jsonInput))
                .andReturn();
        int status = result.getResponse().getStatus(); // need status :200
        Assert.assertEquals(HttpStatus.OK.value(), status); // kiểm tra trạng thái

        String content = result.getResponse().getContentAsString();
        ProductDTO pro = baseConvert.mapFromJson(content,ProductDTO.class);
        Assert.assertEquals("san pham 1",pro.getName());

    }
}
