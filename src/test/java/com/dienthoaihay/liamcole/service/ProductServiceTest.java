package com.dienthoaihay.liamcole.service;
import com.dienthoaihay.liamcole.converter.ProductConverter;
import com.dienthoaihay.liamcole.entity.CategoryEntity;
import com.dienthoaihay.liamcole.entity.ProductEntity;

import com.dienthoaihay.liamcole.entity.TagEntity;
import com.dienthoaihay.liamcole.prototype.ProductDTO;
import com.dienthoaihay.liamcole.repository.CategoryRepository;
import com.dienthoaihay.liamcole.repository.ProductRepository;
import com.dienthoaihay.liamcole.repository.TagRepository;
import com.dienthoaihay.liamcole.service.impl.ProductService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.Mock;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


@RunWith(MockitoJUnitRunner.class)

public class ProductServiceTest {
    @Mock
    private ProductConverter productConverter;
    @Mock
    private CategoryRepository categoryRepository;
    @Mock
    private ProductRepository productRepository;
    @Mock
    private TagRepository tagRepository;
    @InjectMocks // tạo thể hiện của lớp để đưa các mock hoặc spy vào
    private ProductService productService;
    // TEST FOR SAVE()
    @Test
    public void whenSave_thenReturnProduct() throws Exception{
        Long[] idTags = {1L,2L};
        ProductDTO productDTO = new ProductDTO("iphone 12","iphone-12","hello","213213","dien-thoai",idTags);

        Mockito.when(productService.save(productDTO)).thenReturn(productDTO);

        ProductDTO pro =productService.save(productDTO);
        Assert.assertEquals("iphone 12",pro.getName());
        // update
        productDTO.setId(1L);
        Mockito.when(productService.save(productDTO)).thenReturn(productDTO);
        ProductDTO pro2 =productService.save(productDTO);

        Assert.assertEquals("iphone 12",pro2.getName());

    }
//    @Test
//    public void when_getAll_product_return_listProduct(){
//        List<ProductDTO> listProduct = new ArrayList<>();
//        CategoryEntity categoryEntity = new CategoryEntity(1L,"Điện thoại","dien-thoai");
//        List<ProductEntity> allProducts = IntStream.range(1, 10).mapToObj(i -> new ProductEntity((long)i,"iphone 12","iphone-12","hello","2321321",categoryEntity))
//                .collect(Collectors.toList());
//        for (ProductEntity item : allProducts) {
//            ProductDTO dto = productConverter.toDTO(item);
//            listProduct.add(dto);
//        }
//
//        Mockito.when(productService.findAll()).thenReturn(listProduct);
//        List<ProductDTO> listResult = listProduct.stream().limit(100).collect(Collectors.toList());
//        Assertions.assertEquals(productService.findAll().get(0).getName(),"Điện thoại");
//
//    }
}