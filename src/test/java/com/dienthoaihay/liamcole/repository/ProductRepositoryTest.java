package com.dienthoaihay.liamcole.repository;

import com.dienthoaihay.liamcole.entity.CategoryEntity;
import com.dienthoaihay.liamcole.entity.ProductEntity;
import com.dienthoaihay.liamcole.entity.TagEntity;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Matchers.any;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductRepositoryTest {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private TagRepository tagRepository;
    @Autowired
    TestEntityManager entityManager;
    @Test
    public void test_find_one_by_id (){
        CategoryEntity categoryEntity = new CategoryEntity(1L,"Điện thoại","dien-thoai");
        categoryEntity = entityManager.persistAndFlush(categoryEntity);
        Long[] idTags = {1L,2L};
        ProductEntity productEntity = new ProductEntity();
        productEntity.setId(1L);
        productEntity.setName("hello");
        productEntity.setCode("code");
        productEntity.setPrice("23232");
        productEntity.setThumbnail("images/img");
        productEntity.setCategories(categoryEntity);
        for(int i =0; i<idTags.length;i++){
            Long k = idTags[i];
            TagEntity tag = tagRepository.findOneById(k);
            productEntity.getTags().add(tag);
        }
        // Merge vào database H2
        productEntity= entityManager.merge(productEntity);
        entityManager.flush();

        ProductEntity pro = productRepository.findOneById(productEntity.getId());
        Assert.assertSame(pro,productEntity);
    }
}